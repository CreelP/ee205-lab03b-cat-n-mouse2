#!/bin/bash

DEFAULT_MAX_NUMBER=10
THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}
THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))


#echo The max value is $THE_MAX_VALUE
#############################################################

echo Okay cat, I\'m thinking of a number from 1 to $DEFAULT_MAX_NUMBER. 


while ((catGuess != THE_NUMBER_IM_THINKING_OF)); do
   read -p "Make a guess: " catGuess
   if ((catGuess == 0)); then
      echo Number must be greater than 0
   elif ((catGuess < MIN_VALUE)); then
      echo You must enter a number that\'s greater than 1!
   elif ((catGuess > THE_MAX_VALUE)); then
      echo You must enter a number tha\'s less than the max value!
   elif ((catGuess < THE_NUMBER_IM_THINKING_OF)); then
      echo No cat... the number I\'m thinking of is larger than $catGuess
   elif ((catGuess > THE_NUMBER_IM_THINKING_OFF)); then
      echo No cat... the number I\'m thinking of is smaller than $catGuess
   fi


done


echo You got me
echo "    |\__/,|   (| "
echo "  _.|o o  |_   ))"
echo "-(((---(((--------"


echo The number was $THE_NUMBER_IM_THINKING_OF
###############################################################################


